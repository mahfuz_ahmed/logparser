package net.therap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author mahfuz.ahmed
 * @since 3/2/20
 */
public final class Utils {
    private static final int HOURS = 24;

    public static int getResponseTime(String regex, Item item, String line) {
        int responseTime = 0;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            String responseTimeString = matcher.group();
            String[] splittedResponseTime = responseTimeString.split("=");
            responseTimeString = splittedResponseTime[1].replaceAll("\\D+", "");
            responseTime = Integer.parseInt(responseTimeString);
        }
        return responseTime;
    }

    public static void countGetPost(String regex, Item item, String line) {
        Pattern pattern = Pattern.compile(", (G|P),");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            String getPostString = matcher.group().toString();
            String[] splittedGetPostString = getPostString.split(" ");
            if (splittedGetPostString[1].equals("P,")) {
                item.setPostCount(item.getPostCount() + 1);
            } else {
                item.setGetCount(item.getGetCount() + 1);
            }
        }
    }

    public static String getTimeFormatted(int startTime, int endTime) throws ParseException {
        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh.mm a");
        Date _24HourStartTime = _24HourSDF.parse(startTime + ":00:00");
        Date _24HourEndTime = _24HourSDF.parse(endTime + ":00:00");
        String startTimeString = _12HourSDF.format(_24HourStartTime);
        String endTimeString = _12HourSDF.format(_24HourEndTime);

        String groupedTime = startTimeString + " - " + endTimeString;

        return groupedTime;
    }

    public static int[] getTimes(String regex, String line) {
        int[] times = new int[2];
        Pattern pattern = Pattern.compile("\\d\\d:\\d\\d:\\d\\d");
        Matcher matcher = pattern.matcher(line);

        if (matcher.find()) {
            String timeString = matcher.group();
            String[] splittedTimeString = timeString.split(":");
            times[0] = Integer.parseInt(splittedTimeString[0]);
            times[1] = (times[0] + 1) % HOURS;
        }

        return times;
    }

    public static void matchURI(String regex, Item item, String line) {
        Pattern pattern = Pattern.compile("\\[\\/(.*)\\]");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            String URIString = matcher.group();
            item.matchURI(URIString);
        }
    }
}
