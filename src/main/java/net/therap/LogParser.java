package net.therap;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.*;

public class LogParser {
    private static final int HOURS = 24;

    public static void main(String[] args) throws ParseException {
        String formattedTime = null;
        LinkedHashMap<String, Item> items = new LinkedHashMap<String, Item>();

        init(items);

        try {
            Scanner scanner = new Scanner(new File(args[0]));

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();

                int times[] = Utils.getTimes("\\d\\d:\\d\\d:\\d\\d", line);

                formattedTime = Utils.getTimeFormatted(times[0], times[1]);

                if (!items.containsKey(formattedTime)) {
                    Item item = new Item(formattedTime);
                    items.put(formattedTime, item);
                }
                Item item = items.get(formattedTime);

                Utils.countGetPost(", (G|P),", item, line);

                Utils.matchURI("\\[\\/(.*)\\]", item, line);

                int responseTime = Utils.getResponseTime("time=\\d*ms", item, line);
                item.setTotalResponseTime(item.getTotalResponseTime() + responseTime);

            }

            System.out.format("%-30s%-30s%-30s%-30s\n", "Time", "GET/POST Count", "Unique URI Count",
                    "Total Response Time");

                if (args.length == 2 && args[1].equals("--sort")) {
                    List<Item> itemsList = new ArrayList<Item>(items.values());

                    Collections.sort(itemsList, new Comparator<Item>() {
                        @Override
                        public int compare(Item o1, Item o2) {
                            int item1Count = o1.getGetCount() + o1.getPostCount();
                            int item2Count = o2.getGetCount() + o2.getPostCount();

                            return item2Count - item1Count;
                        }
                    });

                    items.clear();
                    for (Item item : itemsList) {
                        items.put(item.getTime(), item);
                    }
                }


            for (HashMap.Entry<String, Item> entry : items.entrySet()) {
                Item item = entry.getValue();
                System.out.format("%-30s%-30s%-30s%-30s\n", item.getTime(), item.getGetCount() + "/" + item.getPostCount(),
                        item.getURICount(), item.getTotalResponseTime());
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void init(LinkedHashMap<String, Item> items) throws ParseException {
        for (int i = 0; i < 24; i++) {
            String time = Utils.getTimeFormatted(i, i + 1);
            items.put(time, new Item(time));
        }
    }
}
