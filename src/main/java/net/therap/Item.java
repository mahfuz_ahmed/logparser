package net.therap;

import java.util.Comparator;
import java.util.HashSet;

/**
 * @author mahfuz.ahmed
 * @since 3/2/20
 */
public class Item {
    private String time;
    private int getCount;
    private int postCount;
    private int URICount;
    private int totalResponseTime;
    private HashSet<String> URIs;

    public Item(String time) {
        this.time = time;
        this.getCount = 0;
        this.postCount = 0;
        this.URICount = 0;
        this.totalResponseTime = 0;
        URIs = new HashSet<String>();
    }

    public void matchURI(String URI) {
        if(!URIs.contains(URI)) {
            URIs.add(URI);
            URICount++;
        }
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getGetCount() {
        return getCount;
    }

    public void setGetCount(int getCount) {
        this.getCount = getCount;
    }

    public int getPostCount() {
        return postCount;
    }

    public void setPostCount(int postCount) {
        this.postCount = postCount;
    }

    public int getURICount() {
        return URICount;
    }

    public void setURICount(int URICount) {
        this.URICount = URICount;
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(int totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }


}
